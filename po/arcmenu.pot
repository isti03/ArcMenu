# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-09 19:20-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: constants.js:77
msgid "Favorites"
msgstr ""

#: constants.js:78 menulayouts/eleven.js:211 menulayouts/eleven.js:323
#: menulayouts/launcher.js:354 menulayouts/runner.js:117
#: menulayouts/tweaks/tweaks.js:1006
msgid "Frequent Apps"
msgstr ""

#: constants.js:79 menulayouts/eleven.js:58 menulayouts/raven.js:57
#: menulayouts/raven.js:205 menulayouts/tweaks/tweaks.js:565
#: menulayouts/tweaks/tweaks.js:670 menulayouts/tweaks/tweaks.js:978
#: menulayouts/unity.js:58 menulayouts/unity.js:323 menuWidgets.js:1778
msgid "All Programs"
msgstr ""

#: constants.js:80 menulayouts/eleven.js:56 menulayouts/eleven.js:229
#: menulayouts/eleven.js:321 menulayouts/insider.js:191
#: menulayouts/launcher.js:398 menulayouts/plasma.js:140
#: menulayouts/raven.js:55 menulayouts/raven.js:200 menulayouts/raven.js:266
#: menulayouts/tweaks/tweaks.js:1004 menulayouts/unity.js:56
#: menulayouts/unity.js:318 menulayouts/unity.js:400 menulayouts/windows.js:448
#: menuWidgets.js:1270 prefs.js:42 prefs.js:5870 utils.js:219
msgid "Pinned Apps"
msgstr ""

#: constants.js:81 menulayouts/eleven.js:335 menulayouts/raven.js:288
#: menulayouts/unity.js:418
msgid "Recent Files"
msgstr ""

#: constants.js:190 menulayouts/mint.js:233 menulayouts/unity.js:238
#: prefs.js:460
msgid "Log Out"
msgstr ""

#: constants.js:191 menulayouts/mint.js:234 menulayouts/unity.js:239
#: prefs.js:459
msgid "Lock"
msgstr ""

#: constants.js:192 prefs.js:462
msgid "Restart"
msgstr ""

#: constants.js:193 menulayouts/mint.js:235 menulayouts/unity.js:240
#: prefs.js:461
msgid "Power Off"
msgstr ""

#: constants.js:194 prefs.js:463
msgid "Suspend"
msgstr ""

#: constants.js:195 prefs.js:464
msgid "Hybrid Sleep"
msgstr ""

#: constants.js:196 prefs.js:465
msgid "Hibernate"
msgstr ""

#: constants.js:318 menuWidgets.js:3260 prefs.js:5669
msgid "ArcMenu"
msgstr ""

#: constants.js:319
msgid "Brisk"
msgstr ""

#: constants.js:320
msgid "Whisker"
msgstr ""

#: constants.js:321
msgid "GNOME Menu"
msgstr ""

#: constants.js:322
msgid "Mint"
msgstr ""

#: constants.js:323
msgid "Budgie"
msgstr ""

#: constants.js:326
msgid "Unity"
msgstr ""

#: constants.js:327
msgid "Plasma"
msgstr ""

#: constants.js:328
msgid "tognee"
msgstr ""

#: constants.js:329
msgid "Insider"
msgstr ""

#: constants.js:330
msgid "Redmond"
msgstr ""

#: constants.js:331
msgid "Windows"
msgstr ""

#: constants.js:332
msgid "Eleven"
msgstr ""

#: constants.js:335
msgid "Elementary"
msgstr ""

#: constants.js:336
msgid "Chromebook"
msgstr ""

#: constants.js:339 constants.js:356
msgid "Launcher"
msgstr ""

#: constants.js:340
msgid "Runner"
msgstr ""

#: constants.js:341
msgid "GNOME Overview"
msgstr ""

#: constants.js:344 constants.js:355
msgid "Simple"
msgstr ""

#: constants.js:345
msgid "Simple 2"
msgstr ""

#: constants.js:348
msgid "Raven"
msgstr ""

#: constants.js:352
msgid "Traditional"
msgstr ""

#: constants.js:353
msgid "Modern"
msgstr ""

#: constants.js:354
msgid "Touch"
msgstr ""

#: constants.js:357
msgid "Alternative"
msgstr ""

#: menuButton.js:764 menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:2061 prefs.js:280 prefs.js:380
#: prefs.js:550 prefs.js:5982
msgid "ArcMenu Settings"
msgstr ""

#: menuButton.js:770
msgid "Settings Quick Links:"
msgstr ""

#: menuButton.js:776
msgid "Change Menu Layout"
msgstr ""

#: menuButton.js:777
msgid "Modify Pinned Apps"
msgstr ""

#: menuButton.js:778
msgid "Modify Shortcuts"
msgstr ""

#: menuButton.js:779
msgid "Layout Tweaks"
msgstr ""

#: menuButton.js:780 prefs.js:5862
msgid "Button Appearance"
msgstr ""

#: menuButton.js:786 prefs.js:5864
msgid "About"
msgstr ""

#: menuButton.js:801
msgid "Dash to Panel Settings"
msgstr ""

#: menuButton.js:808
msgid "Dash to Dock Settings"
msgstr ""

#: menuButton.js:812
msgid "Ubuntu Dock Settings"
msgstr ""

#: menulayouts/arcmenu.js:196 menulayouts/baseMenuLayout.js:486
#: menulayouts/mint.js:228 menulayouts/plasma.js:186 menulayouts/raven.js:171
#: menulayouts/redmond.js:167 menulayouts/tognee.js:152
#: menulayouts/unity.js:190 menulayouts/unity.js:233 menulayouts/windows.js:148
msgid "Software"
msgstr ""

#: menulayouts/arcmenu.js:196 menulayouts/mint.js:224 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:1248
msgid "Settings"
msgstr ""

#: menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148
msgid "Tweaks"
msgstr ""

#: menulayouts/arcmenu.js:196 menulayouts/mint.js:223 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:2064
msgid "Terminal"
msgstr ""

#: menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:799
msgid "Activities Overview"
msgstr ""

#: menulayouts/baseMenuLayout.js:428 menulayouts/baseMenuLayout.js:472
#: menulayouts/unity.js:227 placeDisplay.js:452
msgid "Home"
msgstr ""

#: menulayouts/baseMenuLayout.js:428 menulayouts/eleven.js:170
#: menulayouts/insider.js:73 menulayouts/mint.js:230 menulayouts/unity.js:228
#: menulayouts/windows.js:75
msgid "Documents"
msgstr ""

#: menulayouts/baseMenuLayout.js:428 menulayouts/eleven.js:163
#: menulayouts/unity.js:229
msgid "Downloads"
msgstr ""

#: menulayouts/baseMenuLayout.js:428
msgid "Music"
msgstr ""

#: menulayouts/baseMenuLayout.js:428
msgid "Pictures"
msgstr ""

#: menulayouts/baseMenuLayout.js:428
msgid "Videos"
msgstr ""

#: menulayouts/baseMenuLayout.js:428 menulayouts/plasma.js:149
#: menulayouts/unity.js:235 placeDisplay.js:237 placeDisplay.js:261
#: prefs.js:328 prefs.js:456
msgid "Computer"
msgstr ""

#: menulayouts/baseMenuLayout.js:428 menulayouts/baseMenuLayout.js:480
#: menulayouts/plasma.js:336 menulayouts/windows.js:214 prefs.js:329
#: prefs.js:457
msgid "Network"
msgstr ""

#: menulayouts/baseMenuLayout.js:489 placeDisplay.js:722 prefs.js:330
#: prefs.js:458
msgid "Trash"
msgstr ""

#: menulayouts/launcher.js:130
msgid "More"
msgstr ""

#: menulayouts/launcher.js:161
msgid "All"
msgstr ""

#: menulayouts/launcher.js:167 menulayouts/launcher.js:352
#: menulayouts/plasma.js:145 menuWidgets.js:1590 menuWidgets.js:3143
msgid "Applications"
msgstr ""

#: menulayouts/launcher.js:298 menulayouts/launcher.js:361
msgid "Search"
msgstr ""

#: menulayouts/launcher.js:399 menuWidgets.js:2994 prefs.js:249
msgid "Type to search…"
msgstr ""

#: menulayouts/mint.js:232 menulayouts/unity.js:237
msgid "Files"
msgstr ""

#: menulayouts/plasma.js:153 menuWidgets.js:1292
msgid "Leave"
msgstr ""

#: menulayouts/plasma.js:305 menulayouts/windows.js:289 prefs.js:5872
msgid "Application Shortcuts"
msgstr ""

#: menulayouts/plasma.js:309 menulayouts/windows.js:293
msgid "Places"
msgstr ""

#: menulayouts/plasma.js:320 menulayouts/tweaks/tweaks.js:913
#: menulayouts/tweaks/tweaks.js:1044 menulayouts/windows.js:198
msgid "Bookmarks"
msgstr ""

#: menulayouts/plasma.js:328 menulayouts/windows.js:206
msgid "Devices"
msgstr ""

#: menulayouts/plasma.js:371 menuWidgets.js:1325
msgid "Session"
msgstr ""

#: menulayouts/plasma.js:380 menuWidgets.js:1335
msgid "System"
msgstr ""

#: menulayouts/raven.js:268 menulayouts/unity.js:402
msgid "Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:55 menuWidgets.js:1695 menuWidgets.js:1732
#: prefs.js:2665
msgid "Back"
msgstr ""

#: menulayouts/tweaks/tweaks.js:147
msgid "Category Activation"
msgstr ""

#: menulayouts/tweaks/tweaks.js:153
msgid "Mouse Click"
msgstr ""

#: menulayouts/tweaks/tweaks.js:154
msgid "Mouse Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:173
msgid "Avatar Icon Shape"
msgstr ""

#: menulayouts/tweaks/tweaks.js:178
msgid "Circular"
msgstr ""

#: menulayouts/tweaks/tweaks.js:179
msgid "Square"
msgstr ""

#: menulayouts/tweaks/tweaks.js:194 menulayouts/tweaks/tweaks.js:309
msgid "Searchbar Location"
msgstr ""

#: menulayouts/tweaks/tweaks.js:200 menulayouts/tweaks/tweaks.js:315
msgid "Bottom"
msgstr ""

#: menulayouts/tweaks/tweaks.js:201 menulayouts/tweaks/tweaks.js:316
#: menulayouts/tweaks/tweaks.js:507
msgid "Top"
msgstr ""

#: menulayouts/tweaks/tweaks.js:214
msgid "Flip Layout Horizontally"
msgstr ""

#: menulayouts/tweaks/tweaks.js:231
msgid "Disable User Avatar"
msgstr ""

#: menulayouts/tweaks/tweaks.js:249
msgid "Show Applications Grid"
msgstr ""

#: menulayouts/tweaks/tweaks.js:269
msgid "Disable Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:285
msgid "Disable Pinned Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:328
msgid "Activate on Hover"
msgstr ""

#: menulayouts/tweaks/tweaks.js:344
msgid "Show Application Descriptions"
msgstr ""

#: menulayouts/tweaks/tweaks.js:360
msgid "Selected Button Border Color"
msgstr ""

#: menulayouts/tweaks/tweaks.js:378
msgid "Selected Button Background Color"
msgstr ""

#: menulayouts/tweaks/tweaks.js:397 prefs.js:1833 prefs.js:3079 prefs.js:3460
#: prefs.js:3557 prefs.js:4510 prefs.js:4655 prefs.js:4882 prefs.js:5105
msgid "Restore Defaults"
msgstr ""

#: menulayouts/tweaks/tweaks.js:398 prefs.js:1834 prefs.js:3080 prefs.js:3461
#: prefs.js:3558 prefs.js:4511 prefs.js:4656 prefs.js:4883 prefs.js:5106
msgid "Restore the default settings on this page"
msgstr ""

#: menulayouts/tweaks/tweaks.js:436 menulayouts/tweaks/tweaks.js:588
#: menulayouts/tweaks/tweaks.js:704
msgid "Save"
msgstr ""

#: menulayouts/tweaks/tweaks.js:455
msgid "Brisk Menu Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:483
msgid "Enable Activities Overview Shortcut"
msgstr ""

#: menulayouts/tweaks/tweaks.js:502
msgid "Runner Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:508
msgid "Centered"
msgstr ""

#: menulayouts/tweaks/tweaks.js:518
msgid "Show Frequent Apps"
msgstr ""

#: menulayouts/tweaks/tweaks.js:536
msgid "Inherit Shell Theme Popup Gap"
msgstr ""

#: menulayouts/tweaks/tweaks.js:559 menulayouts/tweaks/tweaks.js:664
#: menulayouts/tweaks/tweaks.js:968 menulayouts/tweaks/tweaks.js:995
msgid "Default Screen"
msgstr ""

#: menulayouts/tweaks/tweaks.js:564 menulayouts/tweaks/tweaks.js:669
#: utils.js:205
msgid "Home Screen"
msgstr ""

#: menulayouts/tweaks/tweaks.js:607
msgid "Unity Layout Buttons"
msgstr ""

#: menulayouts/tweaks/tweaks.js:616
msgid "Button Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:625 menulayouts/tweaks/tweaks.js:735
msgid "Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:635 menulayouts/tweaks/tweaks.js:745
#: prefs.js:890
msgid "None"
msgstr ""

#: menulayouts/tweaks/tweaks.js:646 menulayouts/tweaks/tweaks.js:756
msgid "Adjust the position of the separator in the button panel"
msgstr ""

#: menulayouts/tweaks/tweaks.js:693
msgid "Mint Layout Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:726
msgid "Shortcut Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:894 menulayouts/tweaks/tweaks.js:1025
msgid "External Devices"
msgstr ""

#: menulayouts/tweaks/tweaks.js:900 menulayouts/tweaks/tweaks.js:1031
msgid "Show all connected external devices in ArcMenu"
msgstr ""

#: menulayouts/tweaks/tweaks.js:919 menulayouts/tweaks/tweaks.js:1050
msgid "Show all Nautilus bookmarks in ArcMenu"
msgstr ""

#: menulayouts/tweaks/tweaks.js:931 menulayouts/tweaks/tweaks.js:1062
msgid "Extra Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:954
msgid "Nothing Yet!"
msgstr ""

#: menulayouts/tweaks/tweaks.js:975
msgid "Choose the default screen for tognee Layout"
msgstr ""

#: menulayouts/tweaks/tweaks.js:977 menulayouts/tweaks/tweaks.js:1005
msgid "Categories List"
msgstr ""

#: menulayouts/tweaks/tweaks.js:1002
msgid "Choose the default screen for ArcMenu"
msgstr ""

#: menulayouts/tweaks/tweaks.js:1084
msgid "Enable Weather Widget"
msgstr ""

#: menulayouts/tweaks/tweaks.js:1102
msgid "Enable Clock Widget"
msgstr ""

#: menulayouts/windows.js:383
msgid "Frequent"
msgstr ""

#: menuWidgets.js:131
msgid "Current Windows:"
msgstr ""

#: menuWidgets.js:152
msgid "New Window"
msgstr ""

#: menuWidgets.js:165
msgid "Launch using Integrated Graphics Card"
msgstr ""

#: menuWidgets.js:166
msgid "Launch using Discrete Graphics Card"
msgstr ""

#: menuWidgets.js:210
msgid "Delete Desktop Shortcut"
msgstr ""

#: menuWidgets.js:223
msgid "Create Desktop Shortcut"
msgstr ""

#: menuWidgets.js:244
msgid "Remove from Favorites"
msgstr ""

#: menuWidgets.js:251
msgid "Add to Favorites"
msgstr ""

#: menuWidgets.js:273 menuWidgets.js:324
msgid "Unpin from ArcMenu"
msgstr ""

#: menuWidgets.js:286
msgid "Pin to ArcMenu"
msgstr ""

#: menuWidgets.js:298
msgid "Show Details"
msgstr ""

#: menuWidgets.js:317
msgid "Open Folder Location"
msgstr ""

#: menuWidgets.js:1258
msgid "Configure Runner Layout"
msgstr ""

#: menuWidgets.js:1281
msgid "Extras"
msgstr ""

#: menuWidgets.js:1383
msgid "Categories"
msgstr ""

#: menuWidgets.js:1393
msgid "Users"
msgstr ""

#: menuWidgets.js:1648 menuWidgets.js:1665
msgid "All Apps"
msgstr ""

#: menuWidgets.js:2341
msgid "New"
msgstr ""

#: menuWidgets.js:3248
msgid "Show Applications"
msgstr ""

#: menuWidgets.js:3454
msgid "Add world clocks…"
msgstr ""

#: menuWidgets.js:3455
msgid "World Clocks"
msgstr ""

#: menuWidgets.js:3563
msgid "Weather"
msgstr ""

#: menuWidgets.js:3683
msgid "Select a location…"
msgstr ""

#: menuWidgets.js:3696
msgid "Loading…"
msgstr ""

#: menuWidgets.js:3706
msgid "Go online for weather information"
msgstr ""

#: menuWidgets.js:3708
msgid "Weather information is currently unavailable"
msgstr ""

#: placeDisplay.js:138
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:153
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:326
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: placeDisplay.js:732 placeDisplay.js:738
msgid "Empty Trash"
msgstr ""

#: prefs.js:52 prefs.js:722 prefs.js:1363 prefs.js:1568 prefs.js:2652
#: prefs.js:3110 prefs.js:3494 prefs.js:3561 prefs.js:3862 prefs.js:4562
#: prefs.js:4659 prefs.js:4886 prefs.js:5109
msgid "Apply"
msgstr ""

#: prefs.js:65 prefs.js:4896
msgid "Add More Apps"
msgstr ""

#: prefs.js:72
msgid "Browse a list of all applications to add to your Pinned Apps list."
msgstr ""

#: prefs.js:103 prefs.js:4705 prefs.js:4932
msgid "Add Custom Shortcut"
msgstr ""

#: prefs.js:110
msgid "Create a custom shortcut to add to your Pinned Apps list."
msgstr ""

#: prefs.js:236
msgid "Add to your Pinned Apps"
msgstr ""

#: prefs.js:238
msgid "Change Selected Pinned App"
msgstr ""

#: prefs.js:240
msgid "Select Application Shortcuts"
msgstr ""

#: prefs.js:242
msgid "Select Directory Shortcuts"
msgstr ""

#: prefs.js:309 prefs.js:722
msgid "Add"
msgstr ""

#: prefs.js:381 prefs.js:1313
msgid "Run Command..."
msgstr ""

#: prefs.js:382
msgid "Show All Applications"
msgstr ""

#: prefs.js:431
msgid "Default Apps"
msgstr ""

#: prefs.js:435 prefs.js:515
msgid "System Apps"
msgstr ""

#: prefs.js:511
msgid "Presets"
msgstr ""

#: prefs.js:630 prefs.js:632
msgid "Edit Pinned App"
msgstr ""

#: prefs.js:630 prefs.js:632 prefs.js:634 prefs.js:636
msgid "Add a Custom Shortcut"
msgstr ""

#: prefs.js:634
msgid "Edit Shortcut"
msgstr ""

#: prefs.js:636
msgid "Edit Custom Shortcut"
msgstr ""

#: prefs.js:643
msgid "Shortcut Name:"
msgstr ""

#: prefs.js:658
msgid "Icon:"
msgstr ""

#: prefs.js:670
msgid "Browse..."
msgstr ""

#: prefs.js:675 prefs.js:2135 prefs.js:2139
msgid "Select an Icon"
msgstr ""

#: prefs.js:707
msgid "Terminal Command:"
msgstr ""

#: prefs.js:714
msgid "Shortcut Path:"
msgstr ""

#: prefs.js:755 prefs.js:5858
msgid "General"
msgstr ""

#: prefs.js:759
msgid "Panel and Dock Options"
msgstr ""

#: prefs.js:776
msgid "Activities Hot Corner Options"
msgstr ""

#: prefs.js:786 prefs.js:1260
msgid "Modify Activities Hot Corner"
msgstr ""

#: prefs.js:794
msgid "Modify the action of the Activities Hot Corner"
msgstr ""

#: prefs.js:806
msgid "Override the default behavoir of the Activities Hot Corner"
msgstr ""

#: prefs.js:825
msgid "Hotkey activation"
msgstr ""

#: prefs.js:832
msgid "Choose a method for the hotkey activation"
msgstr ""

#: prefs.js:834
msgid "Key Release"
msgstr ""

#: prefs.js:835
msgid "Key Press"
msgstr ""

#: prefs.js:851
msgid "Hotkey Options"
msgstr ""

#: prefs.js:861
msgid "Choose a Hotkey for ArcMenu"
msgstr ""

#: prefs.js:870
msgid "Left Super Key"
msgstr ""

#: prefs.js:873
msgid "Set ArcMenu hotkey to Left Super Key"
msgstr ""

#: prefs.js:876
msgid "Right Super Key"
msgstr ""

#: prefs.js:880
msgid "Set ArcMenu hotkey to Right Super Key"
msgstr ""

#: prefs.js:883
msgid "Custom Hotkey"
msgstr ""

#: prefs.js:887
msgid "Set a custom hotkey for ArcMenu"
msgstr ""

#: prefs.js:894
msgid "Clear ArcMenu hotkey, use GNOME default"
msgstr ""

#: prefs.js:954
msgid "Current Hotkey"
msgstr ""

#: prefs.js:963
msgid "Current custom hotkey"
msgstr ""

#: prefs.js:968
msgid "Modify Hotkey"
msgstr ""

#: prefs.js:971
msgid "Create your own hotkey combination for ArcMenu"
msgstr ""

#: prefs.js:1005
msgid "Display ArcMenu On"
msgstr ""

#: prefs.js:1012
msgid "Choose where to place ArcMenu"
msgstr ""

#: prefs.js:1016
msgid "Dash to Dock"
msgstr ""

#: prefs.js:1020
msgid "Ubuntu Dock"
msgstr ""

#: prefs.js:1023
msgid "Main Panel"
msgstr ""

#: prefs.js:1024
msgid "Dash to Panel"
msgstr ""

#: prefs.js:1033
msgid "Dash to Dock extension not running!"
msgstr ""

#: prefs.js:1033
msgid "Enable Dash to Dock for this feature to work."
msgstr ""

#: prefs.js:1041
msgid "Show Activities Button"
msgstr ""

#: prefs.js:1049
msgid "Show Activities Button in panel"
msgstr ""

#: prefs.js:1076
msgid "Main Panel not found!"
msgstr ""

#: prefs.js:1077
msgid "Dash to Panel extension not running!"
msgstr ""

#: prefs.js:1077
msgid "Enable Dash to Panel for this feature to work."
msgstr ""

#: prefs.js:1097
msgid "Position in Panel"
msgstr ""

#: prefs.js:1104 prefs.js:1149
msgid "Left"
msgstr ""

#: prefs.js:1105
msgid "Position ArcMenu on the left side of the panel"
msgstr ""

#: prefs.js:1108 prefs.js:1150
msgid "Center"
msgstr ""

#: prefs.js:1110
msgid "Position ArcMenu in the center of the panel"
msgstr ""

#: prefs.js:1113 prefs.js:1151
msgid "Right"
msgstr ""

#: prefs.js:1115
msgid "Position ArcMenu on the right side of the panel"
msgstr ""

#: prefs.js:1138
msgid "Menu Alignment"
msgstr ""

#: prefs.js:1147
msgid "Adjust ArcMenu's menu alignment relative to ArcMenu's icon"
msgstr ""

#: prefs.js:1162 prefs.js:1170
msgid "Display ArcMenu on all monitors"
msgstr ""

#: prefs.js:1268
msgid "Activities Hot Corner Action"
msgstr ""

#: prefs.js:1275
msgid "Choose the action of the Activities Hot Corner"
msgstr ""

#: prefs.js:1277
msgid "GNOME Default"
msgstr ""

#: prefs.js:1278
msgid "Disabled"
msgstr ""

#: prefs.js:1279
msgid "Toggle ArcMenu"
msgstr ""

#: prefs.js:1280
msgid "Custom"
msgstr ""

#: prefs.js:1286
msgid "Custom Activities Hot Corner Action"
msgstr ""

#: prefs.js:1286
msgid "Choose from a list of preset commands or use your own terminal command"
msgstr ""

#: prefs.js:1296
msgid "Preset commands"
msgstr ""

#: prefs.js:1302
msgid "Choose from a list of preset Activities Hot Corner commands"
msgstr ""

#: prefs.js:1306
msgid "Show all Applications"
msgstr ""

#: prefs.js:1307
msgid "GNOME Terminal"
msgstr ""

#: prefs.js:1308
msgid "GNOME System Monitor"
msgstr ""

#: prefs.js:1309
msgid "GNOME Calculator"
msgstr ""

#: prefs.js:1310
msgid "GNOME gedit"
msgstr ""

#: prefs.js:1311
msgid "GNOME Screenshot"
msgstr ""

#: prefs.js:1312
msgid "GNOME Weather"
msgstr ""

#: prefs.js:1337
msgid "Terminal Command"
msgstr ""

#: prefs.js:1343
msgid "Set a custom terminal command to launch on active hot corner"
msgstr ""

#: prefs.js:1365
msgid "Apply changes and set new hot corner action"
msgstr ""

#: prefs.js:1436
msgid "Set Custom Hotkey"
msgstr ""

#: prefs.js:1460
msgid "Choose Modifiers"
msgstr ""

#: prefs.js:1476
msgid "Ctrl"
msgstr ""

#: prefs.js:1479
msgid "Super"
msgstr ""

#: prefs.js:1482
msgid "Shift"
msgstr ""

#: prefs.js:1485
msgid "Alt"
msgstr ""

#: prefs.js:1536
msgid "Press any key"
msgstr ""

#: prefs.js:1553
msgid "New Hotkey"
msgstr ""

#: prefs.js:1631
msgid "Menu Button Appearance"
msgstr ""

#: prefs.js:1641
msgid "Appearance"
msgstr ""

#: prefs.js:1647 prefs.js:1769
msgid "Icon"
msgstr ""

#: prefs.js:1648 prefs.js:1738
msgid "Text"
msgstr ""

#: prefs.js:1649
msgid "Icon and Text"
msgstr ""

#: prefs.js:1650
msgid "Text and Icon"
msgstr ""

#: prefs.js:1651
msgid "Hidden"
msgstr ""

#: prefs.js:1680
msgid "Show Arrow"
msgstr ""

#: prefs.js:1700
msgid "Menu Button Padding"
msgstr ""

#: prefs.js:1723
msgid "Default"
msgstr ""

#: prefs.js:1759
msgid "Icon Appearance"
msgstr ""

#: prefs.js:1776
msgid "Browse Icons"
msgstr ""

#: prefs.js:1779
msgid "Choose from a variety of icons"
msgstr ""

#: prefs.js:1797
msgid "Icon Size"
msgstr ""

#: prefs.js:1842
msgid "Menu Button Styling"
msgstr ""

#: prefs.js:1853
msgid "Color"
msgstr ""

#: prefs.js:1860
msgid "Hover Color"
msgstr ""

#: prefs.js:1867
msgid "Active Color"
msgstr ""

#: prefs.js:1874
msgid "Hover Background Color"
msgstr ""

#: prefs.js:1881
msgid "Active Background Color"
msgstr ""

#: prefs.js:1889
msgid "Disable Rounded Corners"
msgstr ""

#: prefs.js:1975
msgid "Override Theme Color"
msgstr ""

#: prefs.js:2038 prefs.js:2174
msgid "ArcMenu Icons"
msgstr ""

#: prefs.js:2126
msgid "Browse for a Custom Icon"
msgstr ""

#: prefs.js:2175
msgid "Distro Icons"
msgstr ""

#: prefs.js:2176
msgid "Custom Icon"
msgstr ""

#: prefs.js:2241
msgid "Legal disclaimer for Distro Icons..."
msgstr ""

#: prefs.js:2323
msgid "Current Menu Layout"
msgstr ""

#: prefs.js:2329
msgid "Available Menu Layouts"
msgstr ""

#: prefs.js:2359
msgid "Click to tweak the current layout"
msgstr ""

#: prefs.js:2460
#, javascript-format
msgid "%s Layout Tweaks"
msgstr ""

#: prefs.js:2513
msgid "Enable Custom Menu Theme"
msgstr ""

#: prefs.js:2523
msgid "Override Menu Theme"
msgstr ""

#: prefs.js:2531
msgid "Override the ArcMenu extension theme"
msgstr ""

#: prefs.js:2674 prefsWidgets.js:771
#, javascript-format
msgid "%s Menu Layouts"
msgstr ""

#: prefs.js:2761
msgid "Menu Height"
msgstr ""

#: prefs.js:2776
msgid "Adjust the menu height"
msgstr ""

#: prefs.js:2776 prefs.js:2824 prefs.js:2870 prefs.js:2917 prefs.js:2939
#: prefs.js:2988 prefs.js:3189
msgid "Certain menu layouts only"
msgstr ""

#: prefs.js:2815
msgid "Left-Panel Width"
msgstr ""

#: prefs.js:2824
msgid "Adjust the left-panel width"
msgstr ""

#: prefs.js:2861
msgid "Right-Panel Width"
msgstr ""

#: prefs.js:2870
msgid "Adjust the right-panel width"
msgstr ""

#: prefs.js:2909
msgid "Large Application Icons"
msgstr ""

#: prefs.js:2917
msgid "Enable large application icons"
msgstr ""

#: prefs.js:2931
msgid "Category Sub Menus"
msgstr ""

#: prefs.js:2939
msgid "Show nested menus in categories"
msgstr ""

#: prefs.js:2955
msgid "Disable Tooltips"
msgstr ""

#: prefs.js:2963
msgid "Disable all tooltips in ArcMenu"
msgstr ""

#: prefs.js:2980 prefs.js:4462
msgid "Enable Vertical Separator"
msgstr ""

#: prefs.js:2988
msgid "Enable a Vertical Separator"
msgstr ""

#: prefs.js:3002 prefs.js:4484
msgid "Separator Color"
msgstr ""

#: prefs.js:3010
msgid "Change the color of all separators"
msgstr ""

#: prefs.js:3028 prefs.js:3036
msgid "Disable Recently Installed Apps Indicator"
msgstr ""

#: prefs.js:3051
msgid "Clear all Applications Marked \"New\""
msgstr ""

#: prefs.js:3059
msgid "Clear All"
msgstr ""

#: prefs.js:3060
msgid "Clear all applications marked \"New\""
msgstr ""

#: prefs.js:3181
msgid "Disable Category Arrows"
msgstr ""

#: prefs.js:3189
msgid "Disable the arrow on category menu items"
msgstr ""

#: prefs.js:3205
msgid "Disable Searchbox Border"
msgstr ""

#: prefs.js:3213
msgid "Disable the border on the searchbox"
msgstr ""

#: prefs.js:3229
msgid "Disable Menu Arrow"
msgstr ""

#: prefs.js:3237
msgid "Disable current theme menu arrow pointer"
msgstr ""

#: prefs.js:3253
msgid "Disable ScrollView Fade Effects"
msgstr ""

#: prefs.js:3261
msgid "Disable all ScrollView fade effects"
msgstr ""

#: prefs.js:3277
msgid "Search Results - Show Descriptions"
msgstr ""

#: prefs.js:3297
msgid "Alphabetize 'All Programs' Category"
msgstr ""

#: prefs.js:3317 prefs.js:3334
msgid "Multi-Lined Labels"
msgstr ""

#: prefs.js:3335
msgid "Enable/Disable multi-lined labels on large application icon layouts."
msgstr ""

#: prefs.js:3351
msgid "Recently Installed Application Indicators"
msgstr ""

#: prefs.js:3362
msgid "Category Indicator Color"
msgstr ""

#: prefs.js:3370
msgid ""
"Change the color of the 'recently installed application' category indicator"
msgstr ""

#: prefs.js:3386
msgid "Application Indicator Label Color"
msgstr ""

#: prefs.js:3394
msgid ""
"Change the background color of the 'recently installed application' "
"indicator label"
msgstr ""

#: prefs.js:3412
msgid "Gap Adjustment"
msgstr ""

#: prefs.js:3424 prefs.js:3441
msgid "Offset ArcMenu by 1px"
msgstr ""

#: prefs.js:3424 prefs.js:3442
msgid "Useful if you notice a 1px gap or overlap between ArcMenu and the panel"
msgstr ""

#: prefs.js:3655 prefs.js:5203 prefsWidgets.js:423
msgid "Enable/Disble"
msgstr ""

#: prefs.js:3682
msgid "Color Theme Name"
msgstr ""

#: prefs.js:3688
msgid "Name:"
msgstr ""

#: prefs.js:3712
msgid "Save Theme"
msgstr ""

#: prefs.js:3736
msgid "Select Themes to Import"
msgstr ""

#: prefs.js:3736
msgid "Select Themes to Export"
msgstr ""

#: prefs.js:3752
msgid "Import"
msgstr ""

#: prefs.js:3752
msgid "Export"
msgstr ""

#: prefs.js:3781
msgid "Select All"
msgstr ""

#: prefs.js:3848 prefs.js:4123
msgid "Manage Presets"
msgstr ""

#: prefs.js:4008 prefs.js:5330
msgid "Menu Theme Presets"
msgstr ""

#: prefs.js:4018
msgid "Current Menu Theme Preset"
msgstr ""

#: prefs.js:4038
msgid "Save as Preset"
msgstr ""

#: prefs.js:4139
msgid "Browse Presets"
msgstr ""

#: prefs.js:4188
msgid "Theme Settings"
msgstr ""

#: prefs.js:4207
msgid "Menu Background Color"
msgstr ""

#: prefs.js:4229
msgid "Menu Foreground Color"
msgstr ""

#: prefs.js:4250
msgid "Font Size"
msgstr ""

#: prefs.js:4280
msgid "Border Color"
msgstr ""

#: prefs.js:4302
msgid "Border Size"
msgstr ""

#: prefs.js:4331
msgid "Active Item Background Color"
msgstr ""

#: prefs.js:4353
msgid "Active Item Foreground Color"
msgstr ""

#: prefs.js:4375
msgid "Corner Radius"
msgstr ""

#: prefs.js:4404
msgid "Menu Arrow Size"
msgstr ""

#: prefs.js:4433
msgid "Menu Displacement"
msgstr ""

#: prefs.js:4669
msgid "Add Default User Directories"
msgstr ""

#: prefs.js:4676
msgid ""
"Browse a list of all default User Directories to add to your Directories "
"Shortcuts"
msgstr ""

#: prefs.js:4712
msgid "Create a custom shortcut to add to your Directories Shortcuts"
msgstr ""

#: prefs.js:4903
msgid "Browse a list of all applications to add to your Application Shortcuts"
msgstr ""

#: prefs.js:4939
msgid "Create a custom shortcut to add to your Application Shortcuts"
msgstr ""

#: prefs.js:5227 prefs.js:5863
msgid "Misc"
msgstr ""

#: prefs.js:5231
msgid "Export or Import Settings"
msgstr ""

#: prefs.js:5241
msgid "All ArcMenu Settings"
msgstr ""

#: prefs.js:5252
msgid "Export or Import All ArcMenu Settings"
msgstr ""

#: prefs.js:5253
msgid ""
"Importing settings from file may replace ALL saved settings.\n"
"This includes all saved pinned apps."
msgstr ""

#: prefs.js:5263
msgid "Import from File"
msgstr ""

#: prefs.js:5266
msgid "Import ArcMenu settings from a file"
msgstr ""

#: prefs.js:5270
msgid "Import settings"
msgstr ""

#: prefs.js:5299
msgid "Export to File"
msgstr ""

#: prefs.js:5302
msgid "Export and save all your ArcMenu settings to a file"
msgstr ""

#: prefs.js:5306
msgid "Export settings"
msgstr ""

#: prefs.js:5341
msgid "Export or Import Menu Theme Presets"
msgstr ""

#: prefs.js:5342
msgid "Menu theme presets are located in the \"Menu Theme\" section"
msgstr ""

#: prefs.js:5352 prefs.js:5359
msgid "Import Theme Preset"
msgstr ""

#: prefs.js:5355
msgid "Import ArcMenu Theme Presets from a file"
msgstr ""

#: prefs.js:5395 prefs.js:5407
msgid "Export Theme Preset"
msgstr ""

#: prefs.js:5398
msgid "Export and save your ArcMenu Theme Presets to a file"
msgstr ""

#: prefs.js:5443
msgid "Reset all Settings"
msgstr ""

#: prefs.js:5444
msgid "Reset all ArcMenu Settings to Default"
msgstr ""

#: prefs.js:5450
msgid "Restore Default Settings?"
msgstr ""

#: prefs.js:5450
msgid "All ArcMenu settings will be reset to the default value."
msgstr ""

#: prefs.js:5557
msgid "ArcMenu Version"
msgstr ""

#: prefs.js:5574
msgid "Git Commit"
msgstr ""

#: prefs.js:5594
msgid "GNOME Version"
msgstr ""

#: prefs.js:5612
msgid "OS"
msgstr ""

#: prefs.js:5644
msgid "Session Type"
msgstr ""

#: prefs.js:5676
msgid "Application Menu Extension for GNOME"
msgstr ""

#: prefs.js:5698
msgid "Donate to the ArcMenu Project"
msgstr ""

#: prefs.js:5706
msgid "Become a Patron of the ArcMenu Project"
msgstr ""

#: prefs.js:5714
msgid "ArcMenu GitLab"
msgstr ""

#: prefs.js:5731
msgid "Developers"
msgstr ""

#: prefs.js:5733
msgid "Translators"
msgstr ""

#: prefs.js:5735
msgid "Contributors"
msgstr ""

#: prefs.js:5737
msgid "Artwork"
msgstr ""

#: prefs.js:5739
msgid "Patrons"
msgstr ""

#: prefs.js:5859
msgid "Menu Layout"
msgstr ""

#: prefs.js:5860
msgid "Menu Theme"
msgstr ""

#: prefs.js:5861
msgid "Customize Menu"
msgstr ""

#: prefs.js:5869
msgid "Menu Settings"
msgstr ""

#: prefs.js:5871
msgid "Directory Shortcuts"
msgstr ""

#: prefs.js:5873
msgid "Power Options"
msgstr ""

#: prefs.js:5874
msgid "Extra Categories"
msgstr ""

#: prefs.js:5875
msgid "Fine-Tune"
msgstr ""

#: prefs.js:5992
msgid "Error - Invalid Shortcut"
msgstr ""

#: prefs.js:5993
msgid "Invalid Shortcut"
msgstr ""

#: prefsWidgets.js:482
msgid "Modify"
msgstr ""

#: prefsWidgets.js:494
msgid "Change"
msgstr ""

#: prefsWidgets.js:516
msgid "Move Up"
msgstr ""

#: prefsWidgets.js:532
msgid "Move Down"
msgstr ""

#: prefsWidgets.js:549
msgid "Delete"
msgstr ""

#: search.js:664
msgid "Searching..."
msgstr ""

#: search.js:666
msgid "No results."
msgstr ""

#: search.js:799
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""

#: utils.js:59
msgid "ArcMenu - Hybrid Sleep Error!"
msgstr ""

#: utils.js:59
msgid "System unable to hybrid sleep."
msgstr ""

#: utils.js:76
msgid "ArcMenu - Hibernate Error!"
msgstr ""

#: utils.js:76
msgid "System unable to hibernate."
msgstr ""

# Found in ArcMenu for GNOME shell 3.36/3.38
msgid "Disable Active Indicator"
msgstr ""

# Found in ArcMenu for GNOME shell 3.36/3.38
msgid "Launch using Dedicated Graphics Card"
msgstr ""
